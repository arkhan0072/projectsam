<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//front end user interaction pages
Route::get('/', function () {
    return view('frontend.index');
})->name('index');
Route::get('/about', function () {
    return view('frontend.about');
})->name('about');
Route::get('/blog', function () {
    return view('frontend.blog');
})->name('blog');
Route::get('/company', function () {
    return view('frontend.company');
})->name('company');
Route::get('/career', function () {
    return view('frontend.career');
})->name('career');
Route::get('/press', function () {
    return view('frontend.press');
})->name('press');
Route::get('/help', function () {
    return view('frontend.help');
})->name('help');
Route::get('/contact', function () {
    return view('frontend.contact');
})->name('contact');
Route::get('/terms', function () {
    return view('frontend.terms');
})->name('terms');
Route::get('/policy', function () {
    return view('frontend.policy');
})->name('policy');
//User Dashboard
Route::group(['prefix' => 'user', 'middleware' => ['auth']], function() {
    Route::get('dashboard',[UserController::class,'index'])->name('udashboard');
    Route::get('courses',[UserController::class,'courses'])->name('ucourses');
    Route::get('membership',[UserController::class,'membership'])->name('umembership');
    Route::get('settings',[UserController::class,'settings'])->name('usettings');
    Route::get('help',[UserController::class,'help'])->name('uhelp');
    Route::get('feedback',[UserController::class,'feedback'])->name('ufeedback');
});
//Admin Dashboard


//Instructor Dashboard
Route::group(['prefix' => 'instructor', 'middleware' => ['auth']], function() {
    Route::get('dashboard',[UserController::class,'index'])->name('idashboard');
    Route::get('courses',[UserController::class,'courses'])->name('icourses');
    Route::get('membership',[UserController::class,'membership'])->name('imembership');
    Route::get('settings',[UserController::class,'settings'])->name('isettings');
    Route::get('help',[UserController::class,'help'])->name('ihelp');
    Route::get('feedback',[UserController::class,'feedback'])->name('ifeedback');
});
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
