<nav>
    <div class="nav nav-tabs tab_crse  justify-content-center">
        <a class="nav-item nav-link active" href="{{route('about')}}">About</a>
        <a class="nav-item nav-link" href="{{route('blog')}}">Blog</a>
        <a class="nav-item nav-link" href="{{route('company')}}">Company</a>
        <a class="nav-item nav-link" href="{{route('career')}}">Careers</a>
        <a class="nav-item nav-link" href="{{route('press')}}">Press</a>
    </div>
</nav>
