@extends('frontend.master')
@section('content')
    <div class="wrapper _bg4586 _new89">
        <div class="_215cd2">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="course_tabs">
                            @include('frontend.includes.innernav')
                        </div>
                        <div class="title129 mt-35 mb-35">
                            <h2>What others are saying</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="_205ef5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-5">
                        <div class="fcrse_3 mt-50">
                            <ul class="blogleft12">
                                <li>
                                    <div class="socl148">
                                        <button class="twiter158" data-href="#" onclick="sharingPopup(this);" id="twitter-share"><i class="uil uil-twitter ic45"></i>Follow</button>
                                        <button class="facebook158" data-href="#" onclick="sharingPopup(this);" id="facebook-share"><i class="uil uil-facebook ic45"></i>Follow</button>
                                    </div>
                                </li>
                                <li>
                                    <div class="help_link">
                                        <p>Learn More</p>
                                        <a href="#">Cursus Help Center</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-7">
                        <div class="press_news">
                            <h2>Cursus in the News</h2>
                            <p>For media interviews and inquiries, please send an email to <a href="#"><span class="__cf_email__" data-cfemail="7c0c0e190f0f3c1f090e0f090f521f1311">[email&#160;protected]</span></a></p>
                            <div class="press_item mt-35">
                                <div class="vdtopress">March 10, 2020</div>
                                <h4>Press News Title</h4>
                                <p class="blog_des">Donec eget arcu vel mauris lacinia vestibulum id eu elit. Nam metus odio, iaculis eu nunc et, interdum mollis arcu. Pellentesque viverra faucibus diam. In sit amet laoreet dolor, vitae fringilla quam...</p>
                                <a href="#" class="press_dt_view">Read More<i class="uil uil-angle-double-right"></i></a>
                            </div>
                            <div class="press_item mt-30">
                                <div class="vdtopress">March 10, 2020</div>
                                <h4>Press News Title</h4>
                                <p class="blog_des">Donec eget arcu vel mauris lacinia vestibulum id eu elit. Nam metus odio, iaculis eu nunc et, interdum mollis arcu. Pellentesque viverra faucibus diam. In sit amet laoreet dolor, vitae fringilla quam...</p>
                                <a href="#" class="press_dt_view">Read More<i class="uil uil-angle-double-right"></i></a>
                            </div>
                            <div class="press_item mt-30">
                                <div class="vdtopress">March 10, 2020</div>
                                <h4>Press News Title</h4>
                                <p class="blog_des">Donec eget arcu vel mauris lacinia vestibulum id eu elit. Nam metus odio, iaculis eu nunc et, interdum mollis arcu. Pellentesque viverra faucibus diam. In sit amet laoreet dolor, vitae fringilla quam...</p>
                                <a href="#" class="press_dt_view">Read More<i class="uil uil-angle-double-right"></i></a>
                            </div>
                            <a href="#" class="allnews_btn">See More News</a>
                        </div>
                        <div class="press_news mb-50">
                            <h2>Press Releases</h2>
                            <p>For Release from Cursus </p>
                            <div class="press_item mt-35">
                                <div class="vdtopress">March 10, 2020</div>
                                <a href="#" class="press_title">Press Release Title</a>
                            </div>
                            <div class="press_item mt-20">
                                <div class="vdtopress">March 10, 2020</div>
                                <a href="#" class="press_title mb-0">Press Release Title</a>
                            </div>
                            <div class="press_item mt-20">
                                <div class="vdtopress">March 10, 2020</div>
                                <a href="#" class="press_title mb-0">Press Release Title</a>
                            </div>
                            <div class="press_item mt-20">
                                <div class="vdtopress">March 10, 2020</div>
                                <a href="#" class="press_title mb-0">Press Release Title</a>
                            </div>
                            <div class="press_item mt-20">
                                <div class="vdtopress">March 10, 2020</div>
                                <a href="#" class="press_title mb-0">Press Release Title</a>
                            </div>
                            <a href="#" class="allnews_btn">See More Press Releases</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

