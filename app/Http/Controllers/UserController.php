<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $user=Auth::user();
        if($user->role==2){
            return view('instructor.index',compact('user'));
        }elseif($user->role==3){
            return view('student.index',compact('user'));
        }elseif ($user->role==1){
            return view('admin.index',compact('user'));
        }else{
            return redirect()->back();
        }
    }
    public function courses()
    {
        $user=Auth::user();
        if($user->role==2){
            return view('instructor.courses',compact('user'));
        }elseif($user->role==3){
            return view('student.courses',compact('user'));
        }elseif ($user->role==1){
            return view('admin.courses',compact('user'));
        }else{
            return redirect()->back();
        }
    }
    public function settings()
    {
        $user=Auth::user();
        if($user->role==2){
            return view('instructor.settings',compact('user'));
        }elseif($user->role==3){
            return view('student.settings',compact('user'));
        }elseif ($user->role==1){
            return view('admin.settings',compact('user'));
        }else{
            return redirect()->back();
        }
    }
    public function membership()
    {
        $user=Auth::user();
        if($user->role==2){
            return view('instructor.membership',compact('user'));
        }elseif($user->role==3){
            return view('student.membership',compact('user'));
        }elseif ($user->role==1){
            return view('admin.membership',compact('user'));
        }else{
            return redirect()->back();
        }
    }
    public function help()
    {
        $user=Auth::user();
        if($user->role==2){
            return view('instructor.help',compact('user'));
        }elseif($user->role==3){
            return view('student.help',compact('user'));
        }elseif ($user->role==1){
            return view('admin.help',compact('user'));
        }else{
            return redirect()->back();
        }
    }
    public function feedback()
    {
        $user=Auth::user();
        if($user->role==2){
            return view('instructor.feedback',compact('user'));
        }elseif($user->role==3){
            return view('student.feedback',compact('user'));
        }elseif ($user->role==1){
            return view('admin.feedback',compact('user'));
        }else{
            return redirect()->back();
        }
    }
}
